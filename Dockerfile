FROM docker:20.10.7

LABEL maintainer="gabrielarellano@gmail.com"

ENV GO_SEMREL_GITLAB_VERSION=0.21.1

ADD https://juhani.gitlab.io/go-semrel-gitlab/download/v$GO_SEMREL_GITLAB_VERSION/release /usr/local/bin/release

RUN chmod a+x /usr/local/bin/release
